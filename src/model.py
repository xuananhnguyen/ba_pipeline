import pandas as pd
import boto3
import logging
import pickle
import mlflow
import mlflow.sklearn
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import precision_recall_fscore_support as score
from data import get_iris_data
from os import environ

logging.basicConfig(level = logging.INFO)

#mlflow
TRACKING_URI = 'http://ec2-user:bapipeline@ec2-18-185-7-231.eu-central-1.compute.amazonaws.com'
mlflow.set_tracking_uri(TRACKING_URI)
client = mlflow.tracking.MlflowClient(TRACKING_URI)
exp_id = environ.get('MLFLOW_EXP')
mlflow.set_experiment(experiment_name = exp_id)

def split_data(test_size, random_state):
    data = get_iris_data()
    X = data.drop(['Species'], axis = 1)
    y = data['Species']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = test_size, random_state = random_state)
    logging.info("split dataset successfull")

    # log param
    mlflow.log_param("test_size", test_size)
    mlflow.log_param("random_state", random_state)

    return X_train, X_test, y_train, y_test

def train_and_evaluate(X_train, X_test, y_train, y_test, neighbors):
    X_train, X_test, y_train, y_test = X_train, X_test, y_train, y_test
    
    model = KNeighborsClassifier(n_neighbors = neighbors)
    logging.info("model built successfull")

    # log n neighbors and model
    mlflow.log_param("n_neighbors", neighbors)
    mlflow.sklearn.log_model(model, "model")

    model.fit(X_train, y_train)
    logging.info("model training successfull")

    y_pred = model.predict(X_test)
    
    precision,recall,fscore,support=score(y_test,y_pred,average='macro')
    logging.info("model evaluation successfull")
    logging.info('Precision : {}'.format(precision))
    logging.info('Recall    : {}'.format(recall))
    logging.info('F-score   : {}'.format(fscore))

    # log metrics
    mlflow.log_metric("precision_score", precision)
    mlflow.log_metric("recall_score", recall)
    mlflow.log_metric("fscore", fscore)

    return model

if __name__ == '__main__':
    split_data = split_data(0.2, 4)
    model = train_and_evaluate(*split_data, 5)

    # pickle model and load to local
    #pickle.dump(model, open("../app/flask/models/model.pkl", "wb"))
    #logging.info("model saved as pickle file into app/flask/models")

    # pickle modle and load to s3 bucket
    client = boto3.client('s3')
    model_pickled = pickle.dumps(model)
    bucketkey = environ.get('S3_PATH')
    client.put_object(Body= model_pickled, Bucket='bamlpipeline', Key= bucketkey)
    logging.info("model saved as pickle file into s3 bucket")