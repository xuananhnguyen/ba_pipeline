import boto3
import logging
import pandas as pd

logging.basicConfig(level = logging.INFO)

def get_iris_data():
    """
    Load the iris data from filesystem

    return:
    iris_df : pandas.DataFrame
        The iris flowers dataset
    """
    
    bucket = 'bamlpipeline'
    filename = 'dataset/Iris.csv'
    
    #load data from s3 bucket
    s3 = boto3.client('s3')
    csv_file = s3.get_object(Bucket=bucket, Key=filename)
    iris_df = pd.read_csv(csv_file['Body'])
    logging.info("Dataset access sucessfull")
    
    # data preprocessing
    iris_df = iris_df.drop(["Id"], axis = 1) 
    logging.info("Dataset preprocessing successfull")
    return iris_df