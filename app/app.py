import numpy as np 
from flask import Flask, request, jsonify, render_template
import pickle
import logging
import boto3
import os, time
from os import environ
from stat import ST_MTIME

logging.basicConfig(level = logging.INFO)

app = Flask(__name__)

bucketkey = environ.get('S3_PATH')

# load file from s3 bucket
s3 = boto3.resource('s3')
model = pickle.loads(s3.Bucket('bamlpipeline').Object(bucketkey).get()['Body'].read())
logging.info("successfully loaded pkl file from s3")

# save file from s3 to local
#client.download_file('bamlpipeline', 'dev/model.pkl', './models/model.pkl')
#print("successfully downloaded from s3 bucket")
#st = os.stat('./models/model.pkl')
#print("file modified:", time.asctime(time.localtime(st[ST_MTIME])))
# deserialize model
#model = pickle.load(open("./models/model.pkl", "rb"))

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/predict", methods=["POST"])
def predict():
    logging.info("Incoming request for prediction")
    features = [request.form.get("sepal_length"),request.form.get("sepal_width"),request.form.get("petal_length"),request.form.get("petal_width")]
    final_features = list(map(float, features))
    logging.info(final_features)
    prediction = model.predict([np.array(final_features)])
    logging.info("Responding with prediction "+"".join(prediction))
    return render_template("index.html", prediction_text ="The Species is "+"".join(prediction))

if __name__ == "__main__":
    app.run(port=8000, debug=True, host='0.0.0.0')